from django.urls import path
from django.views.generic import TemplateView

urlpatterns = [
    path('', TemplateView.as_view(template_name='index.html'), name='frontend_inex'),
    path('create', TemplateView.as_view(template_name='index.html'), name='frontend_create'),
    path('edit', TemplateView.as_view(template_name='index.html'), name='frontend_edit'),
    path('login', TemplateView.as_view(template_name='index.html'), name='frontend_login'),
]
