from rest_framework.renderers import JSONRenderer
from rest_framework.utils import json

class ApiRenderer(JSONRenderer):

    def render(self, data, accepted_media_type=None, renderer_context=None):
        response_dict = {
            'status': 'error',
            'message': {},
        }
        if data.get('data'):
            response_dict['message'] = data.get('data')
        if data.get('status'):
            response_dict['status'] = data.get('status')
        return super(ApiRenderer, self).render(response_dict, accepted_media_type, renderer_context)
