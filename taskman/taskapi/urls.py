from django.urls import path, re_path
from rest_framework_simplejwt import views as jwt_views
from .views import TaskListView, TaskCreateView, TaskUpdateView

urlpatterns = [
    path('', TaskListView.as_view(), name='task_list'),
    path('create/', TaskCreateView.as_view(), name='task_create'),
    path('edit/<int:id>/', TaskUpdateView.as_view(), name='task_create'),
    path('login/', jwt_views.TokenObtainPairView.as_view(), name='login'),
]
