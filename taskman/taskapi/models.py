from django.db import models

task_status_choices = [
      (0, 'Не выполнена'),
      (1, 'Не выполнена, отредактирована админом'),
      (10, 'Выполнена'),
      (11, 'Выполнена, отредактирована админом')
    ]

class Task(models.Model):
    username = models.CharField(blank=False, max_length=16)
    email = models.EmailField(blank=False, max_length=64)
    text = models.TextField(blank=False, max_length=1024)
    status = models.IntegerField(default=0, choices=task_status_choices)
