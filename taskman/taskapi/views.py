from django.shortcuts import render
from rest_framework import generics
from rest_framework.response import Response
from rest_framework import filters, status
from .models import Task
from .serializers import TaskSerializer
from rest_framework.permissions import IsAuthenticated

class TaskListView(generics.ListAPIView):
    queryset = Task.objects.all()
    filter_backends = [ filters.OrderingFilter ]
    ordering_fields = [ 'id', 'username', 'email' ]
    serializer_class = TaskSerializer

    def list(self, request, *args, **kwargs):
        developer = self.request.query_params.get('developer', None)
        if not developer:
          return Response({"status": "Developer name undefined"},
              status=status.HTTP_404_NOT_FOUND)
        return super(TaskListView, self).list(request, *args, **kwargs);

class TaskCreateView(generics.CreateAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer

class TaskUpdateView(generics.UpdateAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    lookup_field = 'id'
    permission_classes = [ IsAuthenticated ]
