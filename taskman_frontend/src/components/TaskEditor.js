import React, { useContext } from 'react';
import { ApiContext } from "../contexts/ApiContext";
import TaskForm from './TaskForm';

export default function TaskEditor({ history }) {

  const { appState, taskUpdate } = useContext(ApiContext);

  if(!appState.authKey)
    history.push('/login');

  const onFinish = (formData) => {
    taskUpdate(formData);
  };

  return (
    <TaskForm task={appState.activeTask} onFinish={onFinish}/>
  )
};
