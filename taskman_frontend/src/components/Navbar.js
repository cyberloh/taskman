import React, { useContext } from 'react';
import { Button, Typography } from 'antd';
import { useLocation } from 'react-router-dom'
import { ApiContext } from "../contexts/ApiContext";

const pageTitles = {
  '/': 'Список заданий',
  '/login': 'Вход',
  '/edit': 'Изменить задание',
  '/create': 'Новое задание'
};

const { Title } = Typography;

export default function Navbar(){

  const  { appState, dispatch } = useContext(ApiContext);

  const pageTitle = pageTitles[useLocation().pathname] || '';

  const onExitClick = () => {
    dispatch({ type: 'logout' });
  };

  return(
    <div className="Navbar">
      <Title level={3}>{pageTitle}</Title>
      {
        appState.authKey !== null
        ? <Button onClick={onExitClick}>Выйти</Button>
        : <Button href='/login'>Войти</Button>
      }
    </div>
  )

};
