import React, { useContext } from 'react';
import { Form, Input, Button, Space, Typography } from 'antd';
import { ApiContext } from "../contexts/ApiContext";

const { Text } = Typography;

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 8 },
};

const tailLayout = {
  wrapperCol: { offset: 8, span: 8 },
};

export default function Login() {

  const { appState, login } = useContext(ApiContext);

  const onFinish = (formData) => {
    login(formData);
  };

  return (
    <>
    <Form
      {...layout}
      name="basic"
      initialValues={{ remember: true }}
      onFinish={onFinish}
    >
      <Form.Item
        label="Имя пользователя"
        name="username"
        rules={[{ required: true, message: 'Пожалуйста, укажите имя пользователя' }]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Пароль"
        name="password"
        rules={[{ required: true, message: 'Пожалуйста, укажите пароль' }]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item {...tailLayout}>
        <Space>
        <Button type="primary" htmlType="submit">
          Войти
        </Button>
        <Button type="default" htmlType="cancel" href="/">
          Отмена
        </Button>
        </Space>
      </Form.Item>
    </Form>
    <Text type="danger">{appState.loginError}</Text>
    </>
  )
};
