import React, { useContext, useEffect } from 'react';
import { ApiContext } from "../contexts/ApiContext";
import { Form, Input, Button, Space, Select, Typography } from 'antd';

const { Text } = Typography;
const { TextArea } = Input;
const { Option } = Select;

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 8 },
};

const tailLayout = {
  wrapperCol: { offset: 8, span: 8 },
};

export default function TaskForm({task, onFinish }) {

  const { appState, dispatch } = useContext(ApiContext);
  const [form] = Form.useForm();

  useEffect(() => {
    form.setFieldsValue({ status: task?.status || 0 });
    dispatch( { type: 'setTasksError', payload: '' } )
  }, []);

  const onStatusChange = statusValue => {
    form.setFieldsValue({ status: parseInt(statusValue) });
  };

  return (
    <>
      <Form
        {...layout}
        name="basic"
        initialValues={task}
        form={form}
        onFinish={onFinish}
      >
        <Form.Item
          label="Имя пользователя"
          name="username"
          rules={[{ required: true, message: 'Пожалуйста, укажите имя пользователя' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Email"
          name="email"
          rules={[{ required: true, type: 'email', message: 'Пожалуйста, укажите email' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Текст"
          name="text"
          rules={[{ required: true, message: 'Пожалуйста, укажите текст задания' }]}
        >
          <TextArea />
        </Form.Item>

        <Form.Item
          label="Статус"
          name="status"
        >
          <Select
            onChange={onStatusChange}
            defaultValue={ task?.status || '0' }
          >
            <Option value='0'>Не выполнена</Option>
            <Option value='1'>Не выполнена, отредактирована админом</Option>
            <Option value='10'>Выполнена</Option>
            <Option value='11'>Выполнена, отредактировна админом</Option>
          </Select>,

        </Form.Item>
        <Form.Item {...tailLayout}>
          <Space>
          <Button type="primary" htmlType="submit">
            Сохранить
          </Button>
          <Button type="default" htmlType="cancel" href="/">
            Отмена
          </Button>
          </Space>
        </Form.Item>
      </Form>
      <Text type="danger">{appState.tasksError}</Text>
    </>
  )
};
