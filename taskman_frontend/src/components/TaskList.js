import React, { useContext } from 'react';
import { ApiContext } from "../contexts/ApiContext";
import { Button, Table, Select } from "antd";

const { Option } = Select;

export default function TaskList({history}){

  const {appState, dispatch } = useContext(ApiContext);

  const onTaskEditClick = task => {
    dispatch({ type: 'setActiveTask', payload: task });
    history.push('/edit');
  };

  const onSortChange = sortValue => {
    dispatch({type: 'setTasksSort', payload: sortValue});
  };

  const onPaginationChange = pageNum => {
    dispatch({ type: 'setTasksPageNum', payload: pageNum });
  };
  
  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id'
    },
    {
      title: 'Пользователь',
      dataIndex: 'username',
      key: 'username'
    },
    {

      title: 'Email',
      dataIndex: 'email',
      key: 'email'
    },
    {

      title: 'Текст',
      dataIndex: 'text',
      key: 'text'
    },
    {

      title: 'Статус',
      dataIndex: 'status',
      key: 'status'
    },
    {
      title: 'Действие',
      dataIndex: 'id',
      key: 'id',
      render: (text, task) => <Button onClick={() => onTaskEditClick(task)}>Изменить</Button>
    }
  ];

  const pagination = {
    defaultCurrent: 1,
    current: appState.tasks?.current_page || 1,
    pageSize: appState.tasks?.page_size || 3,
    total: appState.tasks?.total_items || 0,
    onChange: onPaginationChange
  };

  return(
    <>
    <div className='TasksToolbar'>
      <Button href='/create'>Добавить задание</Button>
      <Select
        value={appState.tasksSort}
        placeholder='Сортировка по полю'
        onChange={onSortChange}
      >
        <Option value="none">нет</Option>
        <Option value="id">id возр.</Option>
        <Option value="-id">id уб.</Option>
        <Option value="username">имя пользователя возр.</Option>
        <Option value="-username">имя пользователя уб.</Option>
        <Option value="email">email возр.</Option>
        <Option value="-email">email yб.</Option>
      </Select>
    </div>
    <Table 
      columns={columns} 
      dataSource={appState.tasks.results} 
      pagination={pagination}
      rowKey='id'
    />
    </>
  );
}
