import React, { createContext, useEffect, useReducer } from "react";
import { reducer, initialState } from "./reducer";
import taskService from '../../services/task.service';

const loadData = fn => {
  const listsToFetch = [
    {
      apiMethod: taskService.tasks,
      actionType: 'setTasks'
    },
  ];
  
  for(let list of listsToFetch)
    list.apiMethod()
      .then(json => fn({
        type: list.actionType,
        payload: json
      }));
};

export const APIContext = createContext({
  state: initialState,
  dispatch: () => null
});

export const APIProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => loadData(dispatch), []);

  return (
    <APIContext.Provider value={[state, dispatch]}>
      { children }
    </APIContext.Provider>
  );
};
