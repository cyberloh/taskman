import localStorageService from '../../services/localstorage.service';

export const initialState = {
  authKey: localStorageService.authKey(),
  tasks: [],
  activeTask: {},
};

export const reducer = (state = initialState, action) => {
  switch(action.type){
    case 'setTasks':
      return { ...state, tasks: action.payload };
    case 'setActiveTask':
      return { ...state, activeTask: action.payload }
    case 'setAuthKey':
      localStorageService.setAuthKey(action.payload);
      return { ...state, authKey: action.payload }
    case 'logout':
      localStorageService.deleteAuthKey();
      return { ...state, authKey: null };
    default:
      return state;
  }
};
