import React, { createContext, useEffect, useReducer } from 'react';
import { useHistory } from 'react-router-dom';
import { reducer, initialState } from "./reducer";
import taskService from '../../services/task.service';
import authService from '../../services/auth.service';

export const ApiContext = createContext();

export const ApiProvider = ({children}) => {

  const history = useHistory();

  const [state, dispatch] = useReducer(reducer, initialState);

  const fetchTasks = () => {
    taskService.tasks(state.tasksPageNum, state.tasksSort)
    .then(tasks => dispatch({type: 'setTasks', payload: tasks}));
  };

  const taskCreate = taskData => {
    taskService.create(taskData)
      .then(() => {
        fetchTasks();
        history.push('/');
      })
      .catch(res => res.then(json => dispatch({ type: 'setTasksError', payload: json?.detail })));
  };

  const taskUpdate = (taskUpdates) => {
    taskService.update(state.activeTask.id, taskUpdates)
      .then(() => {
        fetchTasks();
        history.push('/');
      })
      .catch(res => res.then(json => dispatch({ type: 'setTasksError', payload: json?.detail })));
  };

  const login = credentials => {
    authService.login(credentials)
      .then(res => {
        dispatch({type: 'setAuthKey', payload: res?.access});
        history.push('/');
      })
      .catch(res => res.then(json => dispatch({ type: 'setLoginError', payload: json?.detail })));
  };

  const logout = () => {
    dispatch({ type: 'logout' })
  };

  useEffect(() => fetchTasks(), []);
  useEffect(() => fetchTasks(), [state.tasksSort, state.tasksPageNum]);

  return(
    <ApiContext.Provider
      value={{
        appState: state,
        dispatch,
        fetchTasks,
        taskCreate,
        taskUpdate,
        login,
        logout
      }}
    >
      {children}
    </ApiContext.Provider>
  )
};
