import localStorageService from './localstorage.service';

let baseApiUri = '';

if (process.env.NODE_ENV === 'development')
  baseApiUri = process.env.REACT_APP_DEV_API_URI;

if (process.env.NODE_ENV === 'production')
  baseApiUri = process.env.REACT_APP_PROD_API_URI;

const formSubmit = (endpointUri, data, method) => {

  const headers = {
    Accept: 'application/json'
  };

  if(localStorageService.authKey())
    headers.Authorization = 'JWT ' + localStorageService.authKey();

  const formData = new FormData();
  Object.keys(data).forEach(key => formData.append(key, data[key]));
  
  return fetch(baseApiUri + endpointUri, {
      method,
      headers,
      body: formData
    }
  )
  .then(res => {
    if(!res.ok)
      throw res.json();
    return res.json();
  })
};

const httpService = {

  post(endpointUri, data) {
    return formSubmit(endpointUri, data, 'POST');
  },

  patch(endpointUri, data) {
    return formSubmit(endpointUri, data, 'PATCH');
  },
  
  get(endpointUri){
    return fetch(baseApiUri + endpointUri, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
        },
      }
    )
    .then(res => res.json());
  },
};

export default httpService;
