import httpService from './http.service';

const taskService = {

  tasks(pageNum = 1, sort = 'none'){
    let fetchUri =`?developer=${process.env.REACT_APP_DEVELOPER}&page=${pageNum}`;
    if(sort !== 'none')
      fetchUri += `&ordering=${sort}`;
    return httpService.get(fetchUri);
  },

  update(id, updates){
    return httpService.patch(`edit/${id}/`, updates);
  },

  create(taskData){
    return httpService.post('create/', taskData);
  },

};

export default taskService;
