
let baseApiUri = '';

if (process.env.NODE_ENV === 'development')
  baseApiUri = process.env.REACT_APP_DEV_API_URI;

if (process.env.NODE_ENV === 'production')
  baseApiUri = process.env.REACT_APP_PROD_API_URI;

const post = (endpointUri, data) => {
  return fetch(baseApiUri + endpointUri, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data'
      },
      body: JSON.stringify(data)
    }
  )
  .then(res => res.json());
};

const get = endpointUri => {
  return fetch(baseApiUri + endpointUri, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
      },
    }
  )
  .then(res => res.json());
};

const apiService = {
  get,
  post
};

export default apiService;
  
